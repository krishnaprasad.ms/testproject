package com.dematic.portal.controller;

import com.dematic.portal.util.TeamsMeetingService;
import com.microsoft.graph.models.OnlineMeeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class MeetingController {

    @Autowired
    TeamsMeetingService teamsMeetingService;

//    @GetMapping("/create-meeting")
//    public String createMeeting(@RegisteredOAuth2AuthorizedClient("graph") OAuth2AuthorizedClient authorizedClient) {
//        try {
//            // Create the GraphServiceClient using the OAuth2AuthorizedClient
//            GraphServiceClient<Request> graphClient = GraphServiceClient
//                    .builder()
//                    .authenticationProvider(new TokenCredentialAuthProvider(new CustomTokenCredential(authorizedClient)))
//                    .buildClient();
//
//            Event event = new Event();
//            event.subject = "Team Meeting";
//
//            DateTimeTimeZone start = new DateTimeTimeZone();
//            start.dateTime = "2023-06-01T18:00:00"; // Set the start time
//            start.timeZone = "UTC"; // Set the time zone
//            event.start = start;
//
//            DateTimeTimeZone end = new DateTimeTimeZone();
//            end.dateTime = "2023-06-01T19:00:00"; // Set the end time
//            end.timeZone = "UTC"; // Set the time zone
//            event.end = end;
//
//            event.isOnlineMeeting = true; // Set the online meeting flag
//            event.onlineMeetingProvider = OnlineMeetingProviderType.TEAMS_FOR_BUSINESS; // Specify Teams as the provider
//
//            // Set up participants
//            Attendee attendee1 = new Attendee();
//            attendee1.emailAddress = new EmailAddress();
//            attendee1.emailAddress.address = "participant1@example.com"; // Replace with actual email
//            attendee1.type = AttendeeType.REQUIRED;
//
//            Attendee attendee2 = new Attendee();
//            attendee2.emailAddress = new EmailAddress();
//            attendee2.emailAddress.address = "participant2@example.com"; // Replace with actual email
//            attendee2.type = AttendeeType.REQUIRED;
//
//            event.attendees = Arrays.asList(attendee1, attendee2); // Add attendees to the event
//
//            // Now you can create the event in Microsoft Graph
//            graphClient.users("userId") // Replace with the actual user ID or use "/me"
//                    .events()
//                    .buildRequest()
//                    .post(event);
//
//            return "Meeting created successfully!";
//        } catch (OAuth2AuthenticationException e) {
//            return "Authentication failed: " + e.getMessage();
//        } catch (Exception e) {
//            return "Error creating meeting: " + e.getMessage();
//        }
//    }

    @GetMapping("/create-meeting")
    public String createMeeting() {
        try {
            OnlineMeeting onlineMeeting = teamsMeetingService.createMeetingByEmail("krishnaprasad28835@gmail.com", "meeting subject", "2023-06-01T18:00:00Z", "2023-06-01T19:00:00Z");
            return onlineMeeting.chatInfo.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }

    }

}
