package com.dematic.portal.util;

import com.microsoft.graph.authentication.IAuthenticationProvider;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.models.OnlineMeeting;
import com.microsoft.graph.models.User;
import com.microsoft.graph.requests.GraphServiceClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
public class TeamsMeetingService {

    @Autowired
    private IAuthenticationProvider authenticationProvider;

    public OnlineMeeting createMeeting(String subject, String startDateTime, String endDateTime) {
        GraphServiceClient<Request> graphClient = GraphServiceClient.builder()
                .authenticationProvider(authenticationProvider)
                .buildClient();

        OffsetDateTime start = OffsetDateTime.parse(startDateTime);
        OffsetDateTime end = OffsetDateTime.parse(endDateTime);

        OnlineMeeting meeting = new OnlineMeeting();
        meeting.subject = subject;
        meeting.startDateTime = start;
        meeting.endDateTime = end;

        try {
            OnlineMeeting createdMeeting = graphClient.me().onlineMeetings()
                    .buildRequest()
                    .post(meeting);

            return createdMeeting;
        } catch (ClientException e) {
            throw new RuntimeException("Error creating Microsoft Teams meeting", e);
        }
    }

    public OnlineMeeting createMeeting(String userId, String subject, String startDateTime, String endDateTime) {
        GraphServiceClient<Request> graphClient = GraphServiceClient.builder()
                .authenticationProvider(authenticationProvider)
                .buildClient();

        OffsetDateTime start = OffsetDateTime.parse(startDateTime);
        OffsetDateTime end = OffsetDateTime.parse(endDateTime);

        OnlineMeeting meeting = new OnlineMeeting();
        meeting.subject = subject;
        meeting.startDateTime = start;
        meeting.endDateTime = end;

        try {
            OnlineMeeting createdMeeting = graphClient.users(userId).onlineMeetings()
                    .buildRequest()
                    .post(meeting);

            return createdMeeting;
        } catch (ClientException e) {
            throw new RuntimeException("Error creating Microsoft Teams meeting", e);
        }
    }

    public OnlineMeeting createMeetingByEmail(String email, String subject, String startDateTime, String endDateTime) {
        GraphServiceClient<Request> graphClient = GraphServiceClient.builder()
                .authenticationProvider(authenticationProvider)
                .buildClient();
        User user = graphClient.users(email)
                .buildRequest()
                .get();
        OffsetDateTime start = OffsetDateTime.parse(startDateTime);
        OffsetDateTime end = OffsetDateTime.parse(endDateTime);

        OnlineMeeting meeting = new OnlineMeeting();
        meeting.subject = subject;
        meeting.startDateTime = start;
        meeting.endDateTime = end;

        try {
            OnlineMeeting createdMeeting = graphClient.users(user.id).onlineMeetings()
                    .buildRequest()
                    .post(meeting);

            return createdMeeting;
        } catch (ClientException e) {
            throw new RuntimeException("Error creating Microsoft Teams meeting", e);
        }

    }
}